package lapr.project.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * CalculatorExample Class Unit Testing.
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt> on 24/05/16.
 */
public class CalculatorExampleTest {
	/**
	 * Ensure second operand can assume a negative value.
	 */
	@Test
	public void ensureSecondNegativeOperandWorks() {
		// Given
		int firstOperand = 10;
		int secondOperand = -5;
		int expected = 5;

		//When
		CalculatorExample calculator = new CalculatorExample();
		int result = calculator.sum(firstOperand, secondOperand);

		//Then
		assertEquals(expected, result);
	}

}
